import React from 'react';
import 'react-native-gesture-handler';
import { App } from './src/App';

function Root() {
  return (
    <App />
  )
}

export default Root;
