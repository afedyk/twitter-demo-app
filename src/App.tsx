import { NavigationContainer } from "@react-navigation/native"
import React from "react"
import { Provider } from "react-redux"
import { StatusBar } from "react-native"
import { Store, createStore } from "./store/createStore"
import { default as RootStackScreen } from "./navigation/RootStackScreen"
import { Context as WithAuthContext } from "./services/with-auth"
import { Context as WithTwitterAPIContext } from "./services/with-twitter-api"
import { TwitterAPI } from "./services/twitter-api"
import { TWITTER_ACCESS_CODE } from "./config"
import { Auth } from "./services/auth"

export class App extends React.Component<{}> {
  protected store: Store
  protected auth: Auth
  protected twitterAPI: TwitterAPI

  constructor(props: any) {
    super(props)
    this.store = createStore()
    this.twitterAPI = new TwitterAPI(TWITTER_ACCESS_CODE)
    this.auth = new Auth(this.store, this.twitterAPI)
  }

  componentDidMount() {
    this.auth.init()
      .catch(err => console.error(err))
  }

  render() {
    return (
      <React.Fragment>
        <StatusBar barStyle="default" />
        <Provider store={this.store}>
          <WithTwitterAPIContext.Provider value={this.twitterAPI}>
            <WithAuthContext.Provider value={this.auth}>
              <NavigationContainer>
                <RootStackScreen />
              </NavigationContainer>
            </WithAuthContext.Provider>
          </WithTwitterAPIContext.Provider>
        </Provider>
      </React.Fragment>
    )
  }
}
