import { Tweet, User } from "../store/types"

export class TwitterAPI {

  constructor(protected accessToken: string) { }

  getUserByUsername(username: string) {
    const path = `2/users/by/username/${encodeURIComponent(username)}?user.fields=name,profile_image_url,username,description`

    return this.perform(path, "GET").then(response => parseUser(response.data))
  }

  fetchUserTweets(userId: number, paginationToken?: string) {
    let path = `2/users/${encodeURIComponent(userId)}/tweets?expansions=author_id&user.fields=name,profile_image_url,username,description`

    if (paginationToken) {
      path += `&pagination_token=${encodeURIComponent(paginationToken)}`
    }

    return this.perform(path, "GET").then(response => {
      const users = Array.isArray(response?.includes?.users)
        ? response.includes.users.map((u: any) => parseUser(u))
        : []
      const tweets = Array.isArray(response?.data)
        ? response.data.map((t: any) => parseTweet(t))
        : []
      const meta = parseMeta(response.meta)

      return {
        users,
        tweets,
        meta
      }
    })
  }

  perform(path: string, method: string = "GET", options: RequestInit = {}) {
    const headers = new Headers()

    headers.append("Authorization", `Bearer ${this.accessToken}`)

    const requestOptions: RequestInit = {
      ...options,
      method: method,
      headers: headers,
    }

    return fetch(`https://api.twitter.com/${path}`, requestOptions)
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.errors) {
          throw new Error(data.errors[0]?.detail)
        }

        return data
      })
  }
}

function parseUser(data: any): User {
  if (!data) {
    throw new Error("invalid data")
  }

  return {
    id: Number(data?.id),
    name: String(data?.name),
    username: String(data?.username),
    description: String(data?.description),
    profileImageUrl: String(data.profile_image_url),
  }
}

function parseTweet(data: any): Tweet {
  if (!data) {
    throw new Error("invalid data")
  }

  return {
    id: Number(data?.id),
    authorId: Number(data?.author_id),
    text: String(data?.text),
  }
}

function parseMeta(data: any) {
  return {
    nextPageToken: String(data.next_token ?? "")
  }
}