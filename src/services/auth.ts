import AsyncStorage from "@react-native-async-storage/async-storage"
import { removeMe, setAuthStatus, setMe } from "../store/actions"
import { Store } from "../store/createStore"
import { User } from "../store/types"
import { TwitterAPI } from "./twitter-api"

const USER_STORAGE_KEYS = "$user"

export class Auth {
  store: Store
  twitterAPI: TwitterAPI

  constructor(store: Store, twitterAPI: TwitterAPI) {
    this.store = store
    this.twitterAPI = twitterAPI
  }

  init() {
    return this.dehydrateUser()
      .then(user => {
        this.store.dispatch(setMe(user))
        this.store.dispatch(setAuthStatus("logged_in"))
      })
      .catch(() => {
        this.store.dispatch(setAuthStatus("logged_out"))
      })
  }

  login(username: string) {
    return this.twitterAPI.getUserByUsername(username)
      .then(user => {
        this.rehydrateUser(user)
        this.store.dispatch(setMe(user))
        this.store.dispatch(setAuthStatus("logged_in"))
      })
  }

  logout() {
    return AsyncStorage.removeItem(USER_STORAGE_KEYS)
      .then(() => {
        this.store.dispatch(removeMe())
        this.store.dispatch(setAuthStatus("logged_out"))
      })
  }

  dehydrateUser(): Promise<User> {
    return AsyncStorage.getItem(USER_STORAGE_KEYS).then(value => {
      if (!value) {
        throw new Error("no user in storage")
      }

      const user = JSON.parse(value) as any

      return {
        id: Number(user.id),
        name: String(user.name),
        username: String(user.username),
        description: String(user.description),
        profileImageUrl: String(user.profileImageUrl),
      }
    })
  }

  rehydrateUser(user: User) {
    return AsyncStorage.setItem(USER_STORAGE_KEYS, JSON.stringify(user))
  }
}