import React, { createContext, ComponentType } from "react"
import { TwitterAPI } from "./twitter-api"

export const Context = createContext<TwitterAPI | null>(null)

export interface WithTwitterAPIProps {
  twitterAPI: TwitterAPI
}

export function withTwitterAPI<P extends WithTwitterAPIProps = WithTwitterAPIProps>(WrappedComponent: ComponentType<P>) {
  const name = WrappedComponent.displayName || WrappedComponent.name || 'Component';

  function ComponentWithTwitterAPI(props: any) {
    return (
      <Context.Consumer>{twitterAPI => {
        return (
          <WrappedComponent {...(props as P)} twitterAPI={twitterAPI} />
        )
      }}</Context.Consumer>
    )
  }

  ComponentWithTwitterAPI.displayName = `WithTwitterAPI(${name})`;

  return ComponentWithTwitterAPI
}
