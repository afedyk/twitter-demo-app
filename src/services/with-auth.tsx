import React, { createContext, ComponentType } from "react"
import { Auth } from "./auth"

export const Context = createContext<Auth | null>(null)

export interface WithAuthProps {
  auth: Auth
}

export function withAuth<P extends WithAuthProps = WithAuthProps>(WrappedComponent: ComponentType<P>) {
  const name = WrappedComponent.displayName || WrappedComponent.name || 'Component';

  function ComponentWithAuth(props: any) {
    return (
      <Context.Consumer>{auth => {
        return (
          <WrappedComponent {...(props as P)} auth={auth} />
        )
      }}</Context.Consumer>
    )
  }

  ComponentWithAuth.displayName = `WithAuth(${name})`;

  return ComponentWithAuth
}
