import React from "react"
import { StyleSheet, Text, View } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import { Avatar } from "./Avatar"

interface Props {
  name: string
  text: string
  profileImageUrl: string
  onPress?(): void
}

export class TweetView extends React.Component<Props> {
  render() {
    return (
      <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
        <View style={styles.avatar}>
          <Avatar url={this.props.profileImageUrl} />
        </View>
        <View style={styles.body}>
          <Text style={styles.name}>{this.props.name}</Text>
          <Text style={styles.text}>{this.props.text}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 8,
    paddingHorizontal: 14,
    flexDirection: "row",
    backgroundColor: "#fff",
    borderBottomColor: "#BDBDBD",
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  avatar: {
    marginTop: 6,
    marginRight: 8,
  },
  image: {
    width: 36,
    height: 36,
  },
  body: {
    flex: 1,
  },
  name: {
    fontSize: 18,
    fontWeight: "bold"
  },
  text: {
    fontSize: 16,
    maxWidth: "100%"
  },
})
