import React from "react";
import { ActivityIndicator, View } from "react-native";

export function Loader() {
  return (
    <View style={{ alignContent: "center", padding: 14 }}>
      <ActivityIndicator size={24} color="#1DA1F2" />
    </View>
  )
}
