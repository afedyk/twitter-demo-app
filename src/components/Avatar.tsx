import React from "react"
import { Image, StyleSheet, View } from "react-native"

interface Props {
  url: string
}

export class Avatar extends React.Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={{ uri: this.props.url }} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: 46,
    height: 46,
    borderRadius: 46 / 2,
    overflow: "hidden",
  }
})
