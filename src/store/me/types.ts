import { User } from "../types";

export type MeState = null | User

export const SET_ME = "SET_ME";
export const REMOVE_ME = "REMOVE_ME";

export interface setMeAction {
  type: typeof SET_ME
  payload: User
}

export interface removeMeAction {
  type: typeof REMOVE_ME
}

export type ActionTypes = setMeAction | removeMeAction
