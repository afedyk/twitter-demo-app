import { MeState, ActionTypes, SET_ME, REMOVE_ME } from './types'

const initialState: MeState = null

export function meReducers(state = initialState, action: ActionTypes): MeState {
  switch (action.type) {
    case SET_ME:
      return action.payload

    case REMOVE_ME:
      return null

    default:
      return state
  }
}

