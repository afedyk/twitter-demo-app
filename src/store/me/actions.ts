import { User } from '../types'
import { ActionTypes, REMOVE_ME, SET_ME } from './types'

export function setMe(me: User): ActionTypes {
  return {
    type: SET_ME,
    payload: me
  }
}

export function removeMe(): ActionTypes {
  return {
    type: REMOVE_ME
  }
}
