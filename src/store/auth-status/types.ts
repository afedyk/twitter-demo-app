import { AuthStatus } from "../types";

export type AuthStatusState = AuthStatus

export const SET_AUTH_STATUS = "SET_AUTH_STATUS";

export interface setAuthStatusAction {
  type: typeof SET_AUTH_STATUS
  payload: AuthStatus
}

export type ActionTypes = setAuthStatusAction
