import { ActionTypes, AuthStatusState, SET_AUTH_STATUS } from './types'

const initialState: AuthStatusState = "loading"

export function authStatusReducers(state = initialState, action: ActionTypes): AuthStatusState {
  switch (action.type) {
    case SET_AUTH_STATUS:
      return action.payload
    default:
      return state
  }
}

