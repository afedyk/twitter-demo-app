import { AuthStatus } from '../types'
import { ActionTypes, SET_AUTH_STATUS } from './types'

export function setAuthStatus(payload: AuthStatus): ActionTypes {
  return {
    type: SET_AUTH_STATUS,
    payload
  }
}
