import { StackNavigationState } from "@react-navigation/native";

export type AuthStatus = "logged_in" | "logged_out" | "loading"

export interface Tweet {
  id: number
  text: string
  authorId: number
}

export interface User {
  id: number
  name: string
  description: string
  username: string
  profileImageUrl: string
}
