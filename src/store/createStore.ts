import * as redux from "redux"
import { meReducers } from "./me/reducers"
import { authStatusReducers } from "./auth-status/reducers"

const combinedReducer = redux.combineReducers({
  me: meReducers,
  authStatus: authStatusReducers,
})

export type AppState = ReturnType<typeof combinedReducer>

export type Store = ReturnType<typeof createStore>

export function createStore() {
  return redux.createStore(combinedReducer)
}
