import { RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { Tweet, User } from "../store/types";

export type HomeStackParamList = {
  Home: undefined
  Settings: undefined
}

export type RootStackParamList = {
  Loading: undefined
  Home: undefined
  Details: {
    user: User
    tweet: Tweet
  }
  SignIn: undefined
}

export type HomeStackNavigationProp = StackNavigationProp<RootStackParamList>
export type RootStackNavigationProp = StackNavigationProp<RootStackParamList>
export type SignInScreenNavigationProp = StackNavigationProp<RootStackParamList, "SignIn">
export type SignInScreenRouteProp = RouteProp<RootStackParamList, "SignIn">
export type HomeScreenNavigationProp = StackNavigationProp<RootStackParamList, "Home">
export type HomeScreenRouteProp = RouteProp<RootStackParamList, "Home">
export type DetailsScreenNavigationProp = StackNavigationProp<RootStackParamList, "Details">
export type DetailsScreenRouteProp = RouteProp<RootStackParamList, "Details">
