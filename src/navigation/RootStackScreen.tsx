import React from 'react';
import { connect } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import { RootStackParamList } from './types';
import { AppState } from '../store/createStore';
import { AuthStatus } from '../store/types';
import { HomeStackScreen } from './HomeStackScreen';
import { DetailsScreen } from "../screens/DetailsScreen";
import SignInScreen from '../screens/SignInScreen';
import { LoadingScreen } from '../screens/LoadingScreen';

interface ConnectedProps {
  status: AuthStatus
}

export const RootStack = createStackNavigator<RootStackParamList>()

function RootStackScreen(props: ConnectedProps) {
  switch (props.status) {
    case "logged_in":
      return (
        <RootStack.Navigator mode="modal">
          <RootStack.Screen name="Home" component={HomeStackScreen} options={{ animationEnabled: false, headerBackTitle: "Back" }} />
          <RootStack.Screen name="Details" component={DetailsScreen} options={{ title: "Details", headerBackTitle: "Back" }} />
        </RootStack.Navigator>
      )

    case "logged_out":
      return (
        <RootStack.Navigator>
          <RootStack.Screen name="SignIn" component={SignInScreen} options={{ animationEnabled: false, headerShown: false }} />
        </RootStack.Navigator>
      )

    case "loading":
      return (
        <RootStack.Navigator>
          <RootStack.Screen name="Loading" component={LoadingScreen} options={{ headerShown: false }} />
        </RootStack.Navigator>
      )

    default:
      return console.warn("RootStackScreen: unknown mode was passed"), null
  }
}

const withConnect = connect<ConnectedProps, {}, {}, AppState>(function (state) {
  return {
    status: state.authStatus
  }
})

export default withConnect(RootStackScreen)