import React from 'react'
import { Text } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { HomeStackParamList } from "./types";
import HomeScreen from '../screens/HomeScreen';
import SettingsScreen from '../screens/SettingsScreen';

const HomeStack = createBottomTabNavigator<HomeStackParamList>()

export function HomeStackScreen() {
  return (
    <HomeStack.Navigator initialRouteName="Home" screenOptions={options => {
      return {
        tabBarIcon: params => {
          const icon = options.route.name === "Home" ? "🏡" : "⚙️"

          return <Text>{icon}</Text>
        }
      }
    }}>
      <HomeStack.Screen name="Home" component={HomeScreen} />
      <HomeStack.Screen name="Settings" component={SettingsScreen} />
    </HomeStack.Navigator>
  )
}
