import React from "react"
import { StyleSheet, Text, View } from "react-native";
import { TweetView } from "../components/TweetView";
import { DetailsScreenNavigationProp, DetailsScreenRouteProp } from "../navigation/types";

interface Props {
  navigation: DetailsScreenNavigationProp
  route: DetailsScreenRouteProp
}

export function DetailsScreen(props: Props) {
  return (
    <View style={styles.container}>
      <TweetView
        name={props.route.params.user.name}
        text={props.route.params.tweet.text}
        profileImageUrl={props.route.params.user.profileImageUrl}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})