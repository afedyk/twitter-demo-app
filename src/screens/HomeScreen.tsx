import React from "react"
import { NativeScrollEvent, NativeSyntheticEvent, RefreshControl, SafeAreaView, ScrollView, StyleSheet, Text } from "react-native";
import { connect } from "react-redux";
import { Loader } from "../components/Loader";
import { TweetView } from "../components/TweetView";
import { isCloseToBottom } from "../helpers/is-close-to-bottom";
import { HomeScreenNavigationProp, HomeScreenRouteProp } from "../navigation/types";
import { withTwitterAPI, WithTwitterAPIProps } from "../services/with-twitter-api";
import { AppState } from "../store/createStore";
import { Tweet, User } from "../store/types";

interface Props extends WithTwitterAPIProps, ConnectedProps {
  navigation: HomeScreenNavigationProp
  route: HomeScreenRouteProp
}

interface ConnectedProps {
  userId?: number
}

interface State {
  tweets: Tweet[]
  users: User[]
  nextPageToken: string
  status: "loading" | "loading-more" | "pull-to-refresh" | "idle"
}

class HomeScreen extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      tweets: [],
      users: [],
      nextPageToken: "",
      status: "idle"
    }
  }

  componentDidMount() {
    const userId = this.props.userId

    if (!userId) {
      return
    }

    this.setState({
      status: "loading"
    })

    this.props.twitterAPI.fetchUserTweets(userId)
      .then(data => this.setState({
        tweets: data.tweets,
        users: data.users,
        nextPageToken: data.meta.nextPageToken,
      }))
      .catch(err => {
        console.warn(err)
      })
      .finally(() => this.setState({
        status: "idle"
      }))
  }

  handlePullToRefresh = () => {
    const userId = this.props.userId

    if (!userId) {
      return
    }

    // already load something
    if (this.state.status !== "idle") {
      return
    }

    this.props.twitterAPI.fetchUserTweets(userId)
      .then(data => {
        this.setState({
          tweets: data.tweets,
          users: data.users,
          nextPageToken: data.meta.nextPageToken
        })
      })
      .catch(err => {
        console.warn(err)
      })
      .finally(() => {
        this.setState({
          status: "idle"
        })
      })
  }

  handleScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const userId = this.props.userId

    if (!userId) {
      return
    }

    if (!isCloseToBottom(event.nativeEvent)) {
      return
    }

    // already load something
    if (this.state.status !== "idle") {
      return
    }

    // no more data to get
    if (!this.state.nextPageToken) {
      return
    }

    this.setState({
      status: "loading-more"
    })

    this.props.twitterAPI.fetchUserTweets(userId, this.state.nextPageToken)
      .then(data => {
        this.setState({
          tweets: this.state.tweets.concat(data.tweets),
          users: this.state.users.concat(data.users),
          nextPageToken: data.meta.nextPageToken
        })
      })
      .catch(err => {
        console.warn(err)
      })
      .finally(() => {
        this.setState({
          status: "idle"
        })
      })
  }

  render() {
    return (
      <SafeAreaView style={styles.container} >
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.status === "pull-to-refresh"}
              onRefresh={this.handlePullToRefresh}
            />
          }
          onScroll={this.handleScroll}
        >
          {this.state.tweets.length === 0 ? this.renderEmptyScreen() : this.renderList()}
          {this.renderFooter()}
        </ScrollView>
      </SafeAreaView>
    )
  }

  renderEmptyScreen() {
    if (this.state.status === "idle") {
      return (
        <Text>No tweets</Text>
      )
    }

    return (
      <Loader />
    )
  }

  renderList() {
    const users = new Map(this.state.users.map(u => [u.id, u]))

    return this.state.tweets.map(tweet => {
      const user = users.get(tweet.authorId)

      if (!user) {
        return null
      }

      const handlePress = () => this.props.navigation.navigate("Details", {
        user,
        tweet
      })

      return (
        <TweetView
          key={tweet.id}
          name={user.name}
          text={tweet.text}
          profileImageUrl={user.profileImageUrl}
          onPress={handlePress}
        />
      )
    })
  }

  renderFooter() {
    if (this.state.status !== "loading-more") {
      return null
    }

    return (
      <Loader />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

const withConnect = connect<ConnectedProps, {}, {}, AppState>(function (state: AppState) {
  return {
    userId: state.me?.id
  }
})

export default withConnect(withTwitterAPI(HomeScreen))
