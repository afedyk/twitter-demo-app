import React from "react"
import { Text, SafeAreaView, StyleSheet, TextInput, Button, Animated, View } from "react-native";
import { SignInScreenNavigationProp, SignInScreenRouteProp } from "../navigation/types";
import { withAuth, WithAuthProps } from "../services/with-auth";

interface Props extends WithAuthProps {
  navigation: SignInScreenNavigationProp
  route: SignInScreenRouteProp
}

interface State {
  username: string
  password: string
  isLoading: boolean
}

class SignInScreen extends React.Component<Props, State> {
  shakeAnimation: Animated.Value

  constructor(props: Props) {
    super(props)

    this.shakeAnimation = new Animated.Value(0)
    this.state = {
      username: "",
      password: "",
      isLoading: false
    }
  }

  handleSubmit = () => {
    this.setState({
      isLoading: true
    })

    return Promise.resolve(this.state.username)
      .then(username => {
        if (!username) {
          throw new Error("`username` can't be empty")
        }

        return this.props.auth.login(username)
      })
      .then(() => {
        this.props.navigation.navigate("Home")
      })
      .catch(err => {
        this.shakeForm()
      })
      .finally(() => {
        this.setState({
          isLoading: false
        })
      })
  }

  shakeForm() {
    this.shakeAnimation.setValue(0)

    Animated.timing(this.shakeAnimation, {
      toValue: 1,
      duration: 400,
      useNativeDriver: true
    }).start()
  }

  render() {
    const translateX = this.shakeAnimation.interpolate({
      inputRange: [0, 0.2, 0.4, 0.6, 0.8, 1],
      outputRange: [0, -10, 10, -10, 10, 0]
    })

    const formStyle = [styles.form, {
      transform: [{
        translateX: translateX
      }]
    }]

    return (
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.container}>
          <Text style={styles.title}>Welcome</Text>
          <Text style={styles.subtitle}>Sign in using your Twitter username</Text>
          <Animated.View style={formStyle}>
            <TextInput
              style={styles.input}
              placeholder="Username"
              autoFocus
              onChangeText={username => this.setState({ username })}
              autoCompleteType="username"
              returnKeyType="next"
              value={this.state.username}
              onSubmitEditing={this.handleSubmit}
              autoCorrect={false}
            />
            <TextInput
              style={[styles.input, styles.noBorderTop]}
              placeholder="Password"
              autoCompleteType="password"
              onChangeText={password => this.setState({ password })}
              value={this.state.password}
            />
          </Animated.View>
          <Button onPress={this.handleSubmit} title="Continue" disabled={this.state.isLoading} />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 14,
  },
  title: {
    fontSize: 24,
    marginBottom: 8,
  },
  subtitle: {
    fontSize: 16,
    marginBottom: 14,
  },
  input: {
    fontSize: 16,
    padding: 12,
    borderColor: '#BDBDBD',
    borderWidth: 1,
  },
  noBorderTop: {
    borderTopWidth: 0,
  },
  form: {
    width: "100%",
    marginBottom: 14,
  },
})

export default withAuth(SignInScreen)
