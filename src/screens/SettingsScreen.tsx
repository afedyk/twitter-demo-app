import React from "react"
import { connect } from "react-redux";
import { Button, StyleSheet, Text, View } from "react-native";
import { AppState } from "../store/createStore";
import { User } from "../store/types";
import { withAuth, WithAuthProps } from "../services/with-auth";
import { Loader } from "../components/Loader";
import { Avatar } from "../components/Avatar";

interface ConnectedProps {
  user: User | null
}

function SettingsScreen(props: ConnectedProps & WithAuthProps) {
  if (!props.user) {
    return (
      <Loader />
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.avatar}>
        <Avatar url={props.user.profileImageUrl} />
      </View>
      <View style={styles.body}>
        <Text style={styles.name}>{props.user.name}</Text>
        <Text style={styles.description}>{props.user.description}</Text>
        <Button title="Logout" onPress={() => props.auth.logout()} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    padding: 14,
  },
  avatar: {
    paddingTop: 8,
    paddingRight: 8,
  },
  body: {
    flex: 1
  },
  name: {
    fontSize: 16,
    fontWeight: "bold",
  },
  description: {
    fontSize: 16,
    marginBottom: 14,
  }
})

const withConnect = connect<ConnectedProps, {}, {}, AppState>(function (state: AppState) {
  console.log(state)
  return {
    user: state.me
  }
})

export default withAuth(withConnect(SettingsScreen))
