import { NativeScrollEvent } from "react-native";

const paddingToBottom = 24;

export function isCloseToBottom(event: NativeScrollEvent) {
  return event.layoutMeasurement.height + event.contentOffset.y >= event.contentSize.height - paddingToBottom;
}