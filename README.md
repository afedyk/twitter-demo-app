# Twitter Demo App

## Run locally

```
git clone git@gitlab.com:afedyk/twitter-demo-app.git
cd twitter-demo-app
npm install
react-native run-android // for android
react-native run-ios     // for ios
```

